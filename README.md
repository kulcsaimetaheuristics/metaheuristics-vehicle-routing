## Generate data
`python generate.py 20 5 datasets/test-20-5.in`

## Find solution
### To Dos:
* Parametrize use of GA components (command line parameters)
* Optimally spread routes over all available vehicles
* Write solution to separate stream (e.g. debugging info to cerr or file for output).
## Evaluate assignment
`python evaluator.py datasets/test-20-5.in assignment.txt`

## Run ORTools
`python -m google_ortools -- datasets/test-20-5.in ortools_assignment.txt`