#include <bits/stdc++.h>
#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/lognormal.hpp>
#include <boost/program_options.hpp>

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<double> vd;
typedef vector<ll> vll;
typedef vector<ull> vull;
typedef vector<pii> vpii;
typedef vector<double> vd;
typedef vector<vi> vvi;

default_random_engine re;
bool debug = false;
bool plot = false;

/////////////////////////
// DATA REPRESENTATION //
/////////////////////////

// Represents a single distribution
struct dist_distribution
{
    string type;   // the type of the distribution
    vd parameters; // The  parameters of the distribution

    double ppf(double p)
    { // Percent point function
        if (type == "normal")
        {
            if (this->parameters[1] == 0)
                return 0;
            boost::math::normal dist(this->parameters[0], this->parameters[1]);
            return quantile(dist, p);
        }
        if (type == "lognorm")
        {
            if (this->parameters[1] == 0)
                return 0;
            boost::math::lognormal_distribution<double> dist(this->parameters[0], this->parameters[1]);
            return quantile(dist, p);
        }
        return this->parameters[0];
    }
};

// Returns the number of parameters needed for the given distribution
int number_of_parameters_of_distribution(string dist)
{
    return (dist == "burr") ? 3 : 2;
}

typedef vector<dist_distribution> vdist;
typedef vector<vdist> vvdist;

// Represents a single data set
struct CVRPData
{
    int V;            // The fleet size
    int N;            // The number of requests
    vd s;             // The sizes of the requests
    vvdist dist;      // The distance distributions
    vdist depotDist;  // The distance distributions from the depot to the shipments
    vdist depotDistB; // The distance distributions from the shipments to the depot
};

// Reads in a distribution
void read_distribution(dist_distribution &dist)
{
    cin >> dist.type;
    int n = number_of_parameters_of_distribution(dist.type);
    dist.parameters = vd(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> dist.parameters[i];
    }
}

// Reads in the data file
CVRPData readData()
{
    CVRPData data;
    cin >> data.V;
    cin >> data.N;
    data.depotDist = vdist(data.N);
    data.depotDistB = vdist(data.N);
    dist_distribution garbage;
    read_distribution(garbage);
    for (int i = 0; i < data.N; ++i)
    {
        read_distribution(data.depotDist[i]);
    }
    data.dist = vvdist(data.N, vdist(data.N));
    for (int i = 0; i < data.N; i++)
    {
        read_distribution(data.depotDistB[i]);
        for (int j = 0; j < data.N; j++)
        {
            read_distribution(data.dist[i][j]);
        }
    }
    data.s = vd(data.N);
    for (int i = 0; i < data.N; ++i)
    {
        cin >> data.s[i];
    }
    return data;
}

////////////////////////
// GENETRIC ALGORITHM //
////////////////////////

// Represents the status of a genetic algorithm
struct GeneticAlgorithmStatus
{
    int nrOfGenerationsPassed = 0;
    clock_t startClock = clock();
    vd bestCosts = {};
    vd worstCosts = {};
};

// The configuration of a genetic algorithm
struct GeneticAlgorithmConfiguration
{
    int populationSize;
    bool (*shouldStop)(GeneticAlgorithmStatus);
    double distributionPercentile;
    double mutationProbability;
    double inversionProbability;
    string cluster;
    int binarySearchSteps;
    CVRPData data;
};

// A global configuration
GeneticAlgorithmConfiguration globalConf;

// An individual is represented by its node permutation
struct Individual
{

    vi nodes;     // The nodes of the individual representing the permutation
    double _cost; // An optional cached cost, -1 if not cached

    Individual()
    {
        nodes = {};
        _cost = -1;
    }
    Individual(const vi &nodes)
    {
        this->nodes = nodes;
        _cost = -1;
    }

    // Returns the cost of the individual (it caches this value)
    double cost()
    {
        if (_cost > 0)
            return _cost;
        _cost = calculate_cost();
        return _cost;
    }

    double s(int i)
    {
        return globalConf.data.s[nodes[i]];
    }

    double d(int i, int j)
    {
        double p = globalConf.distributionPercentile;
        if (i < 0) {
            if (j < 0) return 0;
            return globalConf.data.depotDist[nodes[j]].ppf(p);
        } else {
            if (j < 0) return globalConf.data.depotDistB[nodes[i]].ppf(p);
            return globalConf.data.dist[nodes[i]][nodes[j]].ppf(p);
        }
    }

    // Clusters the routes of the individual.
    vvi cluster_routes()
    {
        if (globalConf.cluster == "fill") return cluster_fill();
        else if (globalConf.cluster == "capacity") return cluster_capacity();
        else if (globalConf.cluster == "time") return cluster_time();
        else if (globalConf.cluster == "binary") return cluster_binary_search();
        return cluster_capacity();
    }

    // Clusters by filling vehicles to their max capacity.
    vvi cluster_fill()
    {
        double capacity = 0.0;
        const vd &sizes = globalConf.data.s;
        vvi routes;
        for (const int node : nodes)
        {
            if (capacity < sizes[node])
            {
                routes.emplace_back();
                capacity = 1.0;
            }
            routes.back().push_back(node);
            capacity -= sizes[node];
        }
        return routes;
    }

    vvi cluster_capacity()
    {
        double bestC = 0;
        for (auto size: globalConf.data.s) bestC += size;
        bestC /= (double)globalConf.data.V;
        vvi routes;
        routes.emplace_back();
        double capacity = 0.0;
        for (const int node: nodes) {
            double s = globalConf.data.s[node];
            if (abs(bestC - capacity) < abs(bestC - capacity - s) || capacity + s > 1) {
                routes.emplace_back();
                capacity = 0;
            } else {
                capacity += s;
            }
            routes.back().push_back(node);
        }
        return routes;
    }
    
    vvi cluster_time()
    {
        double bestT = 0;
        for (int i = 0; i < (int)nodes.size()-1; ++i) {
            bestT += globalConf.data.dist[nodes[i]][nodes[i+1]].ppf(globalConf.distributionPercentile);
        }
        bestT /= (double)globalConf.data.V;
        vvi routes;
        routes.emplace_back();
        double capacity = 0.0;
        double T = 0.0;
        for (int i = 0; i < (int)nodes.size()-1; ++i) {
            double s = globalConf.data.s[nodes[i]];
            double t = globalConf.data.dist[nodes[i]][nodes[i+1]].ppf(globalConf.distributionPercentile);
            if (abs(bestT - T) < abs(bestT - T - t) || capacity + s > 1) {
                routes.emplace_back();
                capacity = 0;
                T = 0;
            } else {
                capacity += s;
                T += t;
            }
            routes.back().push_back(nodes[i]);
        }
        routes.back().push_back(nodes.back());
        return routes;
    }

    vvi cluster_binary_search()
    {
        double low = 0, high = 0;
        high += globalConf.data.depotDist[nodes[0]].ppf(globalConf.distributionPercentile);
        for (int i = 0; i < (int)nodes.size()-1; ++i) {
            high += globalConf.data.dist[nodes[i]][nodes[i+1]].ppf(globalConf.distributionPercentile);
        }
        // high += globalConf.data.depotDistB[nodes.back()].ppf(globalConf.distributionPercentile);
        vvi routes;
        for (int k = 0; k < globalConf.binarySearchSteps; ++k) {
            double T = low + (high - low) / 2;
            routes = {};
            routes.emplace_back();
            double capacity = 0.0;
            double time = globalConf.data.depotDist[nodes.front()].ppf(globalConf.distributionPercentile);
            double possible = true;
            for (int i = 0; i < (int)nodes.size()-1; ++i) {
                double s = globalConf.data.s[nodes[i]];
                double t = globalConf.data.dist[nodes[i]][nodes[i+1]].ppf(globalConf.distributionPercentile);
                if (capacity + s > 1 || time + t > T) {
                    if ((int)routes.size() >= globalConf.data.V) {
                        possible = false;
                        break;
                    }
                    routes.emplace_back();
                    capacity = s;
                    time = globalConf.data.depotDist[nodes[i]].ppf(globalConf.distributionPercentile);
                } else {
                    capacity += s;
                    time += t;
                }
                routes.back().push_back(nodes[i]);
            }
            if (capacity + globalConf.data.s[nodes.back()] > 1) possible = false;
            if (possible) {
                high = T;
            } else {
                low = T;
            }
            routes.back().push_back(nodes.back());
        }
        return routes;
    }

    // typedef map<double, double> mdd;
    // typedef vector<mdd> vmdd;
    // typedef vector<vmdd> vvmdd;
    // typedef vector<vvmdd> vvvmdd;
    //
    // vvvmdd memo;
    //
    // This DP clustering doesn't work yet
    //
    // // Clusters using an optimal dynamic programming strategy.
    // vvi cluster_dp()
    // {
    //     for (auto node: nodes) cout << node << " ";
    //     cout << endl;
    //     memo = vvvmdd(globalConf.data.N, vvmdd(21, vmdd(globalConf.data.V+1)));
    //     _cluster_dp(0, 1.0, globalConf.data.V, 0);
    //     vvi routes;
    //     routes.emplace_back();
    //     int i = 0, v = globalConf.data.V;
    //     double c = 1.0, T = 0;
    //     while (i < (int)nodes.size()-1 && c >= 0 && v > 0) {
    //         double t1 = memo[i+1][(int)((c-s(i))*20)][v][(int)(round(T + d(i, i+1)))];
    //         double t2 = memo[i+1][20][v-1][(int)(round(d(-1, i+1)))];
    //         t2 = max(T + d(i, -1), t2);
    //         if (t2 < t1) {
    //             c = 1;
    //             v--;
    //             T = d(-1, i+1);
    //             routes.emplace_back();
    //         } else {
    //             c -= s(i);
    //             T += d(i, i+1);
    //         }
    //         routes.back().push_back(nodes[i]);
    //         i++;
    //     }
    //     routes.back().push_back(nodes[i]);
    //     for (auto route: routes) {
    //         for (auto node: route) cout << node << " -> ";
    //         cout << endl;
    //     }
    //     return routes;
    // }
    //
    // #<{(| Returns the minimum time necessary to cluster v vehicles
    //  * for the nodes starting at node i, where the current vehicle has
    //  * capacity c and the time of the current trip is T.
    //  |)}>#
    // double _cluster_dp(int i, double c, int v, double T)
    // {
    //     cout << i << " " << c << " " << v << " " << T << endl;
    //     // Base cases
    //     if (c < 0) return 1e8;
    //     if (v <= 0) return 1e8;
    //     if (i == globalConf.data.N-1) return T + d(i, -1);
    //     // Option 1: Get the item with the current vehicle
    //     cout << ">\n";
    //     double t1 = _cluster_dp(i+1, c-s(i), v, T + d(i, i+1));
    //     cout << t1 << endl;
    //     cout << "<\n";
    //     // Option 2: Take the next vehicle
    //     cout << ">\n";
    //     double t2 = _cluster_dp(i+1, 1.0, v-1, d(-1, i+1));
    //     cout << t2 << endl;
    //     cout << "<\n";
    //     t2 = max(T + d(i, -1), t2);
    //     // Take best option
    //     double best = min(t1, t2);
    //     cout << "best: " << best << endl;
    //     memo[i][(int)(c*20)][v][(int)(round(T))] = best;
    //     return best;
    // }

    // Calculates the cost of the individual.
    double calculate_cost()
    {
        vvi routes = cluster_routes();
        set<int> done;
        double cost = 0;
        double penalty = 1;
        for (vi route : routes)
        {
            if (route.size() == 0) continue;
            double route_time = 0;
            double capacity = 0;
            dist_distribution depotStart = globalConf.data.depotDist[route.front()];
            route_time += depotStart.ppf(globalConf.distributionPercentile);
            for (unsigned int i = 0; i < route.size() - 1; ++i)
            {
                int u = route[i], v = route[i + 1];
                dist_distribution d = globalConf.data.dist[u][v];
                route_time += d.ppf(globalConf.distributionPercentile);
                capacity += globalConf.data.s[u];
                if (done.find(u) != done.end()) penalty += 1;
                done.insert(u);
            }
            dist_distribution depotReturn = globalConf.data.depotDistB[route.back()];
            route_time += depotReturn.ppf(globalConf.distributionPercentile);
            if (debug) cerr << route_time << endl;
            cost = max(cost, route_time);
            capacity += globalConf.data.s[route.back()];
            if (capacity > 1) penalty += capacity - 1;
            if (done.find(route.back()) != done.end()) penalty += 1;
            done.insert(route.back());
        }
        if ((int)done.size() != globalConf.data.N) penalty += 0.1 * (globalConf.data.N - done.size());
        if (debug) cerr << "cost: " << cost << " penalty: " << penalty << endl;
        return cost * penalty;
    }
};

bool operator<(Individual left, Individual right)
{
    return left.cost() < right.cost();
}

typedef set<Individual> popul;

// Generates a single random individual
Individual generate_individual(GeneticAlgorithmConfiguration conf)
{
    Individual ind;
    ind.nodes = vi(conf.data.N);
    for (int i = 0; (size_t)i < ind.nodes.size(); ++i)
        ind.nodes[i] = i;
    random_shuffle(ind.nodes.begin(), ind.nodes.end());
    return ind;
}

// Generates an initial population
popul initialisePopulation(const GeneticAlgorithmConfiguration conf)
{
    popul population;
    while ((int)population.size() < conf.populationSize)
    {
        Individual ind = generate_individual(conf);
        population.insert(ind);
    }
    return population;
}

// Returns whether the fitter parent should be selected (stochastic output)
bool should_select_fitter_parent()
{
    bernoulli_distribution d(0.75);
    return d(re);
}

// Selects a parent from the population using binary tournament selection (stochastic output)
Individual select_parent(const popul &population)
{
    uniform_int_distribution<int> unif(0, population.size() - 1);
    const Individual parent1 = *next(population.begin(), unif(re));
    const Individual parent2 = *next(population.begin(), unif(re));
    return should_select_fitter_parent() ? min(parent1, parent2) : max(parent1, parent2);
}

// Returns a pair of parents from the population (stochastic output)
pair<Individual, Individual> select_parents(const popul &population)
{
    return make_pair(select_parent(population), select_parent(population));
}

Individual get_exploratory_child(const Individual &parent1, const Individual &parent2, const Individual &o_child, const GeneticAlgorithmConfiguration &conf)
{
    vi stops;
    const vi parent1_stops = parent1.nodes;
    const vi parent2_stops = parent2.nodes;
    const vi o_child_stops = o_child.nodes;
    for (int i = 0; i < conf.data.N; ++i)
    {
        const int p1 = parent1_stops[i];
        const int p2 = parent2_stops[i];
        const int oc = o_child_stops[i];
        if (p1 == p2 || p1 != oc)
        {
            stops.push_back(p1);
        }
        else
        {
            stops.push_back(p2);
        }
    }
    return Individual(stops);
}

void add_to_offspring(vi *current_offspring, int index, int value, const vector<pair<int, int>> &possible_values, const vector<pair<int, int>> &possible_positions)
{
    if (current_offspring->at(index) == value)
    {
        return;
    }
    current_offspring->at(index) = value;
    // other place at which value could be put: other_position (other_value)
    // other place at which alternative value could be put: alternative_position (alternative_value)
    int alternative_value = (possible_values[index].first == value) ? possible_values[index].second : possible_values[index].first;
    int other_position = (possible_positions[value].first == index) ? possible_positions[value].second : possible_positions[value].first;
    int other_value = (possible_values[other_position].first == value) ? possible_values[other_position].second : possible_values[other_position].first;
    int alternative_position = (possible_positions[alternative_value].first == index) ? possible_positions[alternative_value].second : possible_positions[alternative_value].first;
    add_to_offspring(current_offspring, other_position, other_value, possible_values, possible_positions);
    add_to_offspring(current_offspring, alternative_position, alternative_value, possible_values, possible_positions);
}

void generate_temporary_offspring(Individual *best_offspring, double *best_score, const vi &current_offspring, unsigned int depth, const vector<pair<int, int>> &possible_values, const vector<pair<int, int>> &possible_positions, unsigned int start_index)
{
    if (depth > 5)
    {
        vi result = current_offspring;
        for (unsigned int i = start_index; i < result.size(); ++i)
        {
            if (result[i] == -1)
            {
                add_to_offspring(&result, i, possible_values[i].first, possible_values, possible_positions);
            }
        }
        Individual result_c(result);
        if (result_c.cost() < *best_score)
        {
            *best_offspring = result_c;
            *best_score = result_c.cost();
        }
        return;
    }
    for (unsigned int i = start_index; i < current_offspring.size(); ++i)
    {
        if (current_offspring[i] != -1)
            continue;
        vi path1_offspring = current_offspring;
        vi path2_offspring = current_offspring;
        add_to_offspring(&path1_offspring, i, possible_values[i].first, possible_values, possible_positions);
        add_to_offspring(&path2_offspring, i, possible_values[i].second, possible_values, possible_positions);
        generate_temporary_offspring(best_offspring, best_score, path1_offspring, depth + 1, possible_values, possible_positions, i + 1);
        generate_temporary_offspring(best_offspring, best_score, path2_offspring, depth + 1, possible_values, possible_positions, i + 1);
        return;
    }
    Individual c(current_offspring);
    if (c.cost() < *best_score)
    {
        *best_offspring = c;
        *best_score = c.cost();
    }
}

vector<pair<int, int>> get_possible_values_vector(const vi &stops1, const vi &stops2)
{
    vector<pair<int, int>> result;
    for (unsigned int i = 0; i < stops1.size(); ++i)
    {
        result.push_back(make_pair(stops1[i], stops2[i]));
    }
    return result;
}

vector<pair<int, int>> get_possible_positions_vector(const vi &stops1, const vi &stops2)
{
    vector<pair<int, int>> result(stops1.size());
    for (unsigned int i = 0; i < stops1.size(); ++i)
    {
        result[stops1[i]].first = i;
        result[stops2[i]].second = i;
    }
    return result;
}

vi get_common_nodes_vector(const vi &stops1, const vi &stops2)
{
    vi result;
    for (unsigned int i = 0; i < stops1.size(); ++i)
    {
        if (stops1[i] == stops2[i])
            result.push_back(stops1[i]);
        else
            result.push_back(-1);
    }
    return result;
}

// Performs optimised crossover on the parents pair.
pair<Individual, Individual> crossover(const pair<Individual, Individual> &parents, const GeneticAlgorithmConfiguration &conf)
{
    const Individual &parent1 = parents.first;
    const Individual &parent2 = parents.second;
    const vi &stops1 = parent1.nodes;
    const vi &stops2 = parent2.nodes;
    Individual o_child;
    double best_cost = 1e10;
    generate_temporary_offspring(&o_child, &best_cost, get_common_nodes_vector(stops1, stops2), 0, get_possible_values_vector(stops1, stops2), get_possible_positions_vector(stops1, stops2), 0);
    return make_pair(o_child, get_exploratory_child(parent1, parent2, o_child, conf));
}

bool should_inverse()
{
    bernoulli_distribution d(globalConf.inversionProbability);
    return d(re);
}
void inverse(Individual *ind)
{
    uniform_int_distribution<int> unif(0, ind->nodes.size() - 1);
    int left = unif(re);
    int right = unif(re);
    if (left > right)
    {
        swap(left, right);
    }
    vi stops = ind->nodes;
    for (int i = 0; i < (right - left) / 2; ++i)
    {
        swap(stops[i + left], stops[right - i]);
    }
    *ind = Individual(stops);
}
void swap_substring(Individual *ind)
{
    uniform_int_distribution<int> unif(0, ind->nodes.size() - 1);
    int start1;
    int start2;
    do
    {
        start1 = unif(re);
        start2 = unif(re);
    } while (start1 == start2);
    if (start1 > start2)
    {
        swap(start1, start2);
    }
    uniform_int_distribution<int> unif_length1(1, start2 - start1);
    uniform_int_distribution<int> unif_length2(1, ind->nodes.size() - start2);
    int length1 = unif_length1(re);
    int length2 = unif_length2(re);
    const vi old_stops = ind->nodes;
    vi new_stops;
    for (int i = 0; i < start1; ++i)
    {
        new_stops.push_back(old_stops[i]);
    }
    for (int i = 0; i < length2; ++i)
    {
        new_stops.push_back(old_stops[start2 + i]);
    }
    for (int i = start1 + length1; i < start2; ++i)
    {
        new_stops.push_back(old_stops[i]);
    }
    for (int i = 0; i < length1; ++i)
    {
        new_stops.push_back(old_stops[start1 + i]);
    }
    for (int i = start2 + length2; (size_t)i < ind->nodes.size(); ++i)
    {
        new_stops.push_back(old_stops[i]);
    }
    *ind = Individual(new_stops);
}

bool should_mutate() {
    bernoulli_distribution d(globalConf.mutationProbability);
    return d(re);
}

void mutate(Individual* ind){
    if (should_mutate()) {
        if (should_inverse()) {
            inverse(ind);
        } else {
            swap_substring(ind);
        }
    }
}

// Generates mutated offspring
popul generate_offspring(const popul &population, const GeneticAlgorithmConfiguration &conf)
{
    popul offspring;
    for (unsigned int i = 0; i < population.size() / 2; ++i)
    {
        auto parents = select_parents(population);
        auto children = crossover(parents, conf);
        mutate(&children.first);
        mutate(&children.second);
        offspring.insert(children.first);
        offspring.insert(children.second);
    }
    return offspring;
}

void join_population_and_offspring(popul &population, popul &offspring, const GeneticAlgorithmConfiguration conf)
{
    for (Individual ind : offspring)
    {
        population.insert(ind);
    }
    while ((int)population.size() > conf.populationSize)
    {
        // Elitism: erase last element (this is the one with the highest cost)
        population.erase(prev(population.end()));
    }
}

void print_route(vi nodes, ostream& stream)
{
    for (int i = 0; i < (int)nodes.size(); ++i)
    {
        if (i > 0)
            stream << " -> ";
        stream << nodes[i];
    }
    stream << endl;
}

void print_individual(Individual ind, ostream& stream)
{
    if (debug) print_route(ind.nodes, stream);
    auto routes = ind.cluster_routes();
    if (debug) stream << "Routes: " << endl;
    for (auto route : routes)
    {
        print_route(route, stream);
    }
}

// Runs the actual genetic algorithm and returns the best individual
Individual runGeneticAlgorithm(GeneticAlgorithmConfiguration conf)
{
    GeneticAlgorithmStatus status;
    popul population = initialisePopulation(conf);
    while (!conf.shouldStop(status))
    {
        popul offspring = generate_offspring(population, conf);
        join_population_and_offspring(population, offspring, conf);
        status.nrOfGenerationsPassed++;
        Individual best = *population.begin();
        status.bestCosts.push_back(best.cost());
        Individual worst = *population.rbegin();
        status.worstCosts.push_back(worst.cost());
        if (best.cost() < 0.91) debug = true;
        if (debug) cerr << "Generation " << status.nrOfGenerationsPassed << endl;
        if (debug) cerr << "best: " << best.cost() << endl;
        if (plot) cout << best.cost() << "\t" << worst.cost() << endl;
        if (debug) print_individual(best, cerr);
        if (debug) cerr << "worst: " << worst.cost() << endl;
        if (debug) print_individual(worst, cerr);
    }
    return *population.begin();
}

//////////
// MAIN //
//////////

int maxNrGenerations;
bool checkImprovement;
bool checkTime;
int maxTime;

int main(int argc, char *argv[]) {

    // Command line arguments
    namespace po = boost::program_options;
    po::options_description desc("Supported options");
    desc.add_options()
        ("help", "produce help message")
        ("popsize", po::value<int>(), "set population size (default 100)")
        ("percentile", po::value<double>(), "set distribution percentile (default 0.95)")
        ("maxgen", po::value<int>(), "set maximum number of generations (default 500)")
        ("onlymaxgen", "Only use the max number of generations as stop criterion, instead of also checking improvement")
        ("maxtime", po::value<int>(), "set maximum time in seconds (default ignored)")
        ("mutprob", po::value<double>(), "set mutation probability (default 0.5)")
        ("invprob", po::value<double>(), "set inversion probability (default 0.5)")
        ("cluster", po::value<string>(), "set cluster method (fill, capacity, time, binary) (default binary)")
        ("bssteps", po::value<int>(), "set the number of binary search steps for the binary search clustering method (default 10)")
        ("plot", "produce generation plot output, instead of actual output")
        ("debug", "include debugging information")
        ("description", po::value<string>(), "description of the algorithm run")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }

    maxNrGenerations = (vm.count("maxgen") ? vm["maxgen"].as<int>() : 500);
    maxTime = (vm.count("maxtime") ? vm["maxtime"].as<int>() : 0);
    checkTime = (vm.count("maxtime") > 0);
    checkImprovement = (vm.count("onlymaxgen") == 0);
    debug = (vm.count("debug") > 0);
    plot = (vm.count("plot") > 0);

    // Initialise data and configuration
    CVRPData data = readData();
    globalConf.populationSize = (vm.count("popsize") ? vm["popsize"].as<int>() : 100);
    globalConf.distributionPercentile = (vm.count("percentile") ? vm["percentile"].as<double>() : 0.95);
    globalConf.mutationProbability = (vm.count("mutprob") ? vm["mutprob"].as<double>() : 0.5);
    globalConf.inversionProbability = (vm.count("invprob") ? vm["invprob"].as<double>() : 0.5);
    globalConf.cluster = (vm.count("cluster") ? vm["cluster"].as<string>() : "binary");
    globalConf.binarySearchSteps = (vm.count("bssteps") ? vm["bssteps"].as<int>() : 10);
    globalConf.shouldStop = [](GeneticAlgorithmStatus status){
        if (checkTime && double(clock() - status.startClock) / CLOCKS_PER_SEC > maxTime) {
            if (debug) cerr << "Max time elapsed." << endl;
            return true;
        }
        if (status.nrOfGenerationsPassed > maxNrGenerations) {
            if (debug) cerr << "Max number of generations passed." << endl;
            return true;
        }
        if (checkImprovement && status.bestCosts.size() > 100 && status.bestCosts.back() == status.bestCosts[status.bestCosts.size() - 100])
        {
            if (debug) cerr << "Best cost did not improve over the last few transactions." << endl;
            return true;
        }
        return false;
    };
    globalConf.data = data;
    Individual bestIndividual = runGeneticAlgorithm(globalConf);

    if (debug) cerr << "--------------------------------" << endl;
    if (debug) cerr << "Best Individual:" << endl;
    cout << (vm.count("description") ? vm["description"].as<string>() : "no-description") << endl;
    print_individual(bestIndividual, cout);

    return 0;
}
