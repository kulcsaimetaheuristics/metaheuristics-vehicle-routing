import argparse

from generate import generate_distance_distribution
from helpers import Graph


def parse_property_line(key, line):
    parts = line.split(' : ')
    assert parts[0] == key
    return parts[1]


def read_vrp_dataset(stream):
    name = parse_property_line('NAME', stream.readline())
    comment = parse_property_line('COMMENT', stream.readline())
    nb_trucks = int(parse_property_line('NB_TRUCKS', stream.readline()))
    type = parse_property_line('TYPE', stream.readline())
    dimension = int(parse_property_line('DIMENSION', stream.readline()))
    edge_weight_type = parse_property_line('EDGE_WEIGHT_TYPE', stream.readline())
    capacity = float(parse_property_line('CAPACITY', stream.readline()))
    locations = []
    assert stream.readline().strip() == 'NODE_COORD_SECTION'
    for i in range(dimension):
        node_id, x, y = [int(x) for x in stream.readline().split()]
        assert node_id == i + 1
        locations.append((x, y))
    assert stream.readline().strip() == 'DEMAND_SECTION'
    assert int(stream.readline().split()[1]) == 0
    packages = []
    for i in range(dimension - 1):
        packages.append(float(stream.readline().split()[1]) / capacity)
    assert stream.readline().strip() == 'DEPOT_SECTION'
    depot = int(stream.readline())
    assert int(stream.readline()) == -1
    assert stream.readline().strip() == 'EOF'

    locations = [(l[0] - locations[0][0], l[1] - locations[0][1]) for l in locations]
    distance_distributions = [
        [generate_distance_distribution(l1, l2) for l2 in locations]
        for l1 in locations
    ]
    return Graph(nb_trucks, packages, distance_distributions)


def main():
    parser = argparse.ArgumentParser(description='Monte-Carlo evaluation of route assignment.')
    parser.add_argument('vrp_file', type=str)
    parser.add_argument('output', default='test-vrp.in')
    args = parser.parse_args()
    with open(args.vrp_file, 'r') as f:
        graph = read_vrp_dataset(f)
    with open(args.output, 'w') as f:
        graph.write_to_file(f)


if __name__ == "__main__":
    main()
