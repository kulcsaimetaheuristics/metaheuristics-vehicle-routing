#!/bin/bash

echo "Compiling GA"
(cd genetic-algorithm && g++-7 -O2 -std=c++11 -I/usr/local/opt/boost/include -L/usr/local/opt/boost/lib -lboost_program_options-mt -o main main.cpp && cd ..) || exit 1

dataset=./datasets/test-20-5.in
if [ $# -ge 2 ]; then
    dataset="$2"
fi

echo "Running Google OR tools"
start=`date +%s`
python google_ortools.py $dataset assignments/or-assignment.txt --percentile .95 || exit 1
end=`date +%s`
time_limit=$((end-start))
sed -i '.orig' '/^$/d' assignments/or-assignment.txt || exit 1
rm assignments/or-assignment.txt.orig
echo "Google OR Tools ran for $time_limit seconds"
echo "Running GA for $time_limit seconds"
./genetic-algorithm/main --description "$1" --maxgen 1000000 --onlymaxgen --percentile .95 --maxtime $time_limit < $dataset > assignments/ga-assignment.txt || exit 1
echo "Evaluating"
python evaluator.py --plot 1 $dataset "assignments/*-assignment.txt" || exit 1
