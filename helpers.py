import math

import numpy as np
import scipy.stats
EPS = 1e-15

class DistanceDistribution(object):
    @staticmethod
    def distribution_type_string():
        raise NotImplementedError

    @staticmethod
    def nb_parameters():
        raise NotImplementedError

    def parameters(self):
        raise NotImplementedError

    def get_distribution_string(self):
        return "{} {}".format(
            self.distribution_type_string(),
            " ".join([str(x) for x in self.parameters()]))

    def copy(self):
        return self.__class__(*self.parameters())

    def sample(self):
        raise NotImplementedError

    def get_pvalue(self, p):
        raise NotImplementedError


class NormalDistanceDistribution(DistanceDistribution):
    def __init__(self, *args):
        self.mean = args[0]
        self.standard_deviation = args[1]

    @staticmethod
    def distribution_type_string():
        return "normal"

    @staticmethod
    def nb_parameters():
        return 2

    def parameters(self):
        return self.mean, self.standard_deviation

    def sample(self):
        return np.random.normal(self.mean, self.standard_deviation)

    def get_pvalue(self, p):
        if np.isclose(self.standard_deviation, 0):
            return 0
        return scipy.stats.norm.ppf(p, loc=self.mean, scale=self.standard_deviation)


class LogNormalDistanceDistribution(DistanceDistribution):
    def __init__(self, *args):
        self.mean = args[0]
        self.stdev = args[1]

    @staticmethod
    def distribution_type_string():
        return "lognormal"

    @staticmethod
    def nb_parameters():
        return 2

    def parameters(self):
        return self.mean, self.stdev

    def sample(self):
        # return np.random.lognormal(self.mean, self.stdev)
        return scipy.stats.lognorm.rvs(loc=0, scale=math.exp(self.mean), s=self.stdev, size=1)[0]

    def get_pvalue(self, p):
        if np.isclose(self.stdev, 0):
            return 0
        # return scipy.stats.lognorm.ppf(p, self.standard_deviation, loc=self.mean, scale=self.standard_deviation)
        # return math.exp(scipy.stats.norm.ppf(p, loc=self.mean, scale=self.standard_deviation))
        # return scipy.stats.lognorm.ppf(p, self.scale, loc=math.exp(self.location))
        return scipy.stats.lognorm.ppf(p, s=self.stdev, loc=0, scale=math.exp(self.mean))


def read_distance_distribution(line):
    supported_distributions_by_key = {x.distribution_type_string(): x for x in
                                      [LogNormalDistanceDistribution, NormalDistanceDistribution]}
    words = line.split()
    assert len(words) > 0
    distribution_type_key = words[0]
    assert distribution_type_key in supported_distributions_by_key.keys()
    distribution_type = supported_distributions_by_key[distribution_type_key]
    assert len(words) == distribution_type.nb_parameters() + 1
    parameters = [float(x) for x in words[1:]]
    return distribution_type(*parameters)


class Graph:
    def __init__(self, nb_vehicles, deliveries, distance_distributions):
        assert isinstance(nb_vehicles, int)
        assert isinstance(deliveries, list)
        for d in deliveries:
            assert isinstance(d, float)
        assert len(deliveries) + 1 == len(distance_distributions)
        for dd in distance_distributions:
            assert isinstance(dd, list)
            assert len(deliveries) + 1 == len(dd)
            for d in dd:
                assert isinstance(d, DistanceDistribution)

        self._nb_vehicles = nb_vehicles
        self._deliveries = deliveries
        self._distance_distributions = distance_distributions

    def nb_vehicles(self):
        return self._nb_vehicles

    def nb_deliveries(self):
        return len(self._deliveries)

    def delivery_distances(self):
        return [[d.copy() for d in dd] for dd in self._distance_distributions]

    def package_sizes(self):
        return [d for d in self._deliveries]

    def get_package_size(self, location):
        if location <= 0:
            return 0
        return self._deliveries[location - 1]

    def write_to_file(self, f):
        f.write("{}\n".format(self.nb_vehicles()))
        f.write("{}\n".format(self.nb_deliveries()))
        for distributions_from_one_origin in self.delivery_distances():
            for distribution in distributions_from_one_origin:
                f.write("{}\n".format(distribution.get_distribution_string()))
        for p in self.package_sizes():
            f.write("{}\n".format(p))

    @staticmethod
    def from_file(stream):
        nb_vehicles = int(stream.readline())
        nb_deliveries = int(stream.readline())
        distributions = [
            [read_distance_distribution(stream.readline()) for _ in range(nb_deliveries + 1)]
            for _ in range(nb_deliveries + 1)
        ]
        package_sizes = [float(stream.readline()) for _ in range(nb_deliveries)]
        return Graph(nb_vehicles, package_sizes, distributions)

    def get_distribution(self, src, dst):
        return self._distance_distributions[src][dst].copy()


class Route:
    SEPARATOR = " -> "

    def __init__(self, locations):
        assert isinstance(locations, list)
        for l in locations:
            assert isinstance(l, int)
        self._locations = locations

    def edges(self):
        return list(zip([0] + self._locations, self._locations + [0]))

    def locations(self):
        return [l for l in self._locations]

    def copy(self):
        return Route(self.locations())

    @staticmethod
    def from_string(line):
        return Route([int(x) + 1 for x in line.split(Route.SEPARATOR)])

    def write_to_file(self, stream):
        stream.write('{}\n'.format(Route.SEPARATOR.join([str(x - 1) for x in self._locations])))


class RouteAssignment:
    def __init__(self, routes, description):
        assert isinstance(routes, list)
        for r in routes:
            assert isinstance(r, Route)
        assert isinstance(description, str)
        self._description = description
        self._routes = routes

    def routes(self):
        return [r.copy() for r in self._routes]

    def description(self):
        return self._description

    @staticmethod
    def from_file(stream):
        """
        Node indices are zero-based. Depots are not included in the routesreadline
        :param stream:
        :return:
        """
        description = stream.readline()
        return RouteAssignment([Route.from_string(line) for line in stream.readlines() if len(line.strip()) > 0],
                               description)

    def write_to_file(self, stream):
        stream.write("{}\n".format(self._description))
        for r in self.routes():
            r.write_to_file(stream)

    def validate(self, graph):
        all_locations = []
        for r in self.routes():
            all_locations.extend(r.locations())
        assert len(all_locations) == graph.nb_deliveries()
        assert len(set(all_locations)) == graph.nb_deliveries()
        assert all(map(lambda x: 1 <= x <= graph.nb_deliveries(), all_locations))
        assert all(map(lambda route: sum(graph.get_package_size(l) for l in route.locations()) <= 1. + EPS, self.routes()))
