import argparse
import math
import random

from helpers import LogNormalDistanceDistribution, NormalDistanceDistribution, Graph


def generate_location():
    return 2 * random.random() - 1, 2 * random.random() - 1


def generate_package_size():
    # TODO: sampling distribution for package size
    return random.random() / 5


def generate_distance_distribution(origin, destination):
    distance = math.hypot(origin[0] - destination[0], origin[1] - destination[1])
    if distance == 0:
        return NormalDistanceDistribution(0, 0)
    mean = random.uniform(0.8, 1.2) * distance
    if random.random() < .1:
        return LogNormalDistanceDistribution(math.log(mean) if mean > 0 else 0, random.uniform(0, 1))
    std = random.uniform(0, 0.2) * distance
    return NormalDistanceDistribution(mean, std)


def generate_dataset(nb_deliveries, nb_vehicles):
    delivery_locations = [generate_location() for _ in range(nb_deliveries + 1)]

    delivery_distances = [
        [generate_distance_distribution(origin, destination) for destination in delivery_locations] for origin in
        delivery_locations
    ]

    package_sizes = [generate_package_size() for _ in range(nb_deliveries)]
    return Graph(nb_vehicles, package_sizes, delivery_distances)


def main():
    parser = argparse.ArgumentParser(description='Monte-Carlo evaluation of route assignment.')
    parser.add_argument('nb_deliveries', type=int, default=10)
    parser.add_argument('nb_vehicles', type=int, default=3)
    parser.add_argument('output', default='test-1.in')
    args = parser.parse_args()
    graph = generate_dataset(args.nb_deliveries, args.nb_vehicles)
    with open(args.output, 'w') as f:
        graph.write_to_file(f)


if __name__ == "__main__":
    main()
