import argparse
import logging

import math
from ortools.constraint_solver import pywrapcp

from helpers import Graph, RouteAssignment, Route

logging.basicConfig(level=logging.INFO)


# Distance callback

class CreateDistanceCallback(object):
    """Create callback to calculate distances between points."""

    def __init__(self, distance_distributions, p=.70):
        """Initialize distance array."""
        for do in distance_distributions:
            for d in do:
                try:
                    int(DISTANCE_SCALE* d.get_pvalue(p))
                except:
                    print(d.mean)
                    print(d.standard_deviation)
                    raise Exception
        self.matrix = [
            [int(DISTANCE_SCALE * distribution.get_pvalue(p)) for distribution in distributions_from_origin]
            for distributions_from_origin in distance_distributions
        ]

    def Distance(self, from_node, to_node):
        return self.matrix[from_node][to_node]


# Demand callback
class CreateDemandCallback(object):
    """Create callback to get demands at each location."""

    def __init__(self, demands):
        self.matrix = demands

    def Demand(self, from_node, to_node):
        # TODO: check that no demand is expected at the depot (and all locations are being queried).
        return int(math.ceil(self.matrix[from_node] * CAPACITY_SCALE))


CAPACITY_SCALE = 1000
DISTANCE_SCALE = 1000

parser = argparse.ArgumentParser()
parser.add_argument('input_file')
parser.add_argument('output_file')
parser.add_argument('--percentile', type=float, default=.95)


def analyze_solution(graph, routing, assignment, dist_callback):
    # Display solution.
    used_vehicles = 0
    total_cost = 0
    max_dist = 0

    for vehicle_nbr in range(graph.nb_vehicles()):
        index = routing.Start(vehicle_nbr)
        index_next = assignment.Value(routing.NextVar(index))
        route = ''
        route_dist = 0
        route_demand = 0

        while not routing.IsEnd(index_next):
            node_index = routing.IndexToNode(index)
            node_index_next = routing.IndexToNode(index_next)
            route += str(node_index) + " -> "
            # Add the distance to the next node.
            route_dist += dist_callback(node_index, node_index_next)
            # Add demand.
            route_demand += graph.get_package_size(node_index)
            index = index_next
            index_next = assignment.Value(routing.NextVar(index))

        node_index = routing.IndexToNode(index)
        node_index_next = routing.IndexToNode(index_next)
        route += str(node_index) + " -> " + str(node_index_next)
        route_dist += dist_callback(node_index, node_index_next) / DISTANCE_SCALE
        max_dist = max(max_dist, route_dist)

        if route_dist > 0:
            logging.info("Route for vehicle " + str(used_vehicles) + ":\n\n" + route + "\n")
            print("Distance of route " + str(used_vehicles) + ": " + str(route_dist))
            print("Demand met by vehicle " + str(used_vehicles) + ": " + str(route_demand) + "\n")
            used_vehicles += 1
            total_cost += route_dist
    print("Total dist: {}".format(total_cost))
    print("Max dist: {}".format(max_dist))
    print("Objective value: {}".format(assignment.ObjectiveValue()))
    return max_dist


def run_ortools(graph, upper_bound, percentile):
    depot = 0
    # The number of nodes of the VRP graph.nb_deliveries() + 1. The depot (start and end of every route) is node 0.
    routing = pywrapcp.RoutingModel(graph.nb_deliveries() + 1, graph.nb_vehicles(), depot)
    search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
    search_parameters.time_limit_ms = 5000

    logging.info('Configuring distance dimension with upper bound on per-route length')
    dist_between_locations = CreateDistanceCallback(graph.delivery_distances(), percentile)
    dist_callback = dist_between_locations.Distance
    vehicle_distance_dimension_name = "vehicle-distance"
    routing.AddDimension(dist_callback,  # Callback
                         0,  # horizon
                         upper_bound,
                         True,
                         vehicle_distance_dimension_name)

    logging.info('Configuring demand dimension with upper bound on per-route length')
    # Put a callback to the demands.
    demands_at_locations = CreateDemandCallback([0] + graph.package_sizes())
    demands_callback = demands_at_locations.Demand
    null_capacity_slack = 0
    fix_start_cumul_to_zero = True
    capacity = "Capacity"
    vehicle_capacity = CAPACITY_SCALE
    routing.AddDimension(demands_callback, null_capacity_slack, vehicle_capacity,
                         fix_start_cumul_to_zero, capacity)

    logging.info('start solving')
    assignment = routing.SolveWithParameters(search_parameters)
    logging.info('done solving')

    if assignment:
        max_dist = analyze_solution(graph, routing, assignment, dist_callback)

        # EXPORT
        routes = []
        for vehicle_nb in range(graph.nb_vehicles()):
            current = routing.Start(vehicle_nb)
            route = []
            while not routing.IsEnd(current):
                current = assignment.Value(routing.NextVar(current))
                route.append(current)
            routes.append(Route(route[:-1]))
        route_assignment = RouteAssignment(routes, "google-ortools")
        return route_assignment, max_dist
    else:
        print('No solution found.')
        return None


def main():
    args = parser.parse_args()
    with open(args.input_file, 'r') as f:
        graph = Graph.from_file(f)

    # Create routing model.
    if graph.nb_deliveries() > 0:

        left = 0
        right = 99999999999
        best_so_far = None
        while left < right - 2:
            mid = int((left + right) / 2)
            result = run_ortools(graph, mid, args.percentile)
            if result is None:
                logging.info('Did not find a solution with cost at most %s', mid)
                left = mid
            else:
                logging.info('Found a solution with cost: %s', result[1])
                right = result[1]
                best_so_far = result[0]

        if best_so_far is not None:
            with open(args.output_file, 'w') as f:
                best_so_far.write_to_file(f)
        else:
            print('No solution found')
    else:
        print('Specify an instance greater than 0.')


if __name__ == '__main__':
    main()
