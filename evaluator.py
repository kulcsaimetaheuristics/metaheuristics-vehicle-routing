import argparse
import glob
import logging

import matplotlib.pyplot as plt

from helpers import Graph, RouteAssignment


def sample_route_time(graph, route):
    total_travel_time = 0
    for e in route.edges():
        total_travel_time += graph.get_distribution(*e).sample()
    return total_travel_time


def sample_latest_arrival_time(graph, routes_assignment):
    return max([sample_route_time(graph, r) for r in routes_assignment.routes()])


def get_latest_arrival_time_distribution(graph, routes_assignment, nb_samples=100):
    return list(sorted(sample_latest_arrival_time(graph, routes_assignment) for _ in range(nb_samples)))


def main():
    parser = argparse.ArgumentParser(description='Monte-Carlo evaluation of route assignment.')
    parser.add_argument('graph')
    parser.add_argument('route_assignments', type=str)
    parser.add_argument('--plot', type=bool, default=False)
    args = parser.parse_args()
    with open(args.graph, 'r') as f:
        graph = Graph.from_file(f)

    for filename in glob.glob(args.route_assignments):
        with open(filename, 'r') as f:
            assignment = RouteAssignment.from_file(f)
            logging.info('Loaded assignment: %s', assignment.description())
            assignment.validate(graph)
        latest_arrival_times = get_latest_arrival_time_distribution(graph, assignment)
        print(latest_arrival_times)
        print(sum(latest_arrival_times) / 100.0)

        plt.plot(latest_arrival_times, label=assignment.description())
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1))
    plt.tight_layout(h_pad=1)
    if args.plot:
        plt.show()


if __name__ == "__main__":
    main()
